# Simple Flux Query Using Python

## About
This program queries the Dynaspace database using a Python script for data in 
the time window between UNIX time 0 (1970-01-01) and the current time (minus 1 second..). 
It returns the latitude and longditude of shrimp ponds existing in the database in this time
window, in addition to datapoints about activity and area with corresponding timestamp.

All ponds are initialized with a timestamp of unix time 0, so it is possible to query a full 
list of ponds at this timestamp.

The script queries data for one Military Grid Reference System "tile", namely 17NPB.

## Requirements
This program requires Docker to run. If running the python script standalone, 
the libraries found in requirements.txt needs to be installed manually to the 
host machine using pip install.

## How to run

### Add credentials
Modify the python script found in the src/ directory by adding in the access 
token, organisation name, bucket name, host name, and measurement name as 
provided by Dynaspace. 

### Compile docker image:
docker build -t simple_flux_query_using_python_image .

### Run docker image:
docker run simple_flux_query_using_python_image

## Issues
Please create an issue in Gitlab if you find anything wrong with this depository.