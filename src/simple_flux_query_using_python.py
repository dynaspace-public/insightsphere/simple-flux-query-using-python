"""Example script to query InsightSphere.

This is a simple script which queries data from the InsightSphere
database. For access token, organisation name, bucket name, and
measurement name, please contact your contact person in Dynaspace.
"""

# Import required libraries
import configparser
import os

import numpy as np
from influxdb_client import InfluxDBClient

_PWD = os.path.dirname(os.path.realpath(__file__))

_config = configparser.ConfigParser()
_config.read(f"{_PWD}/config.ini")
_client = InfluxDBClient(url=_config['DATABASE']['HOST'],
                         org=_config['DATABASE']['ORG'],
                         token=_config['DATABASE']['TOKEN'])
_query_api = _client.query_api()


def query_db(bucket: str, measurement_name: str) -> np.ndarray:
    """
    Query the database, and return results in a numpy array.

    Use Bind parameters to query InsightSphere. Results are stored in
    a 2D numpy array. The first dimension is variable, and follows
    the number of records returned. The second dimension is constant,
    and corresponds to the number of different fields per record
    (here, 7).

    Parameters
    ----------
    bucket : str
        Bucket name to query
    measurement_name : str
        Name of the measurement to query

    Returns
    -------
    np.ndarray
        2D array storing the results or size (N,7)
    """
    result_array = np.empty((0, 7))  # placeholder

    # parameters = {"_start": datetime.timedelta(hours=-8760)}

    tables = _query_api.query(f"""
        from(bucket:"{bucket}") \
            |> range(start: 1970-01-01T00:00:00Z, stop: -1s)
            |> filter(fn: (r) => r["_measurement"] == "{measurement_name}"
                             and r["tile"] == "17NPB")
    """)

    # create numpy array from query results
    for table in tables:
        for record in table.records:
            result_element = np.array([record["_time"],
                                       record["lat"],
                                       record["lon"],
                                       record["tile"],
                                       record["type"],
                                       record["_field"],
                                       record["_value"]])
            result_array = np.append(result_array, [result_element], axis=0)
    return result_array


if __name__ == "__main__":

    # client api setup

    # Query the database for numpy array
    query_result = query_db(_config['DATABASE']['BUCKET'],
                            _config['DATA']['MEASUREMENT_NAME'])
    print(query_result)
