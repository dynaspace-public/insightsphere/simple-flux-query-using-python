# set base image (host OS)
FROM python:3.9

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install -r requirements.txt

# set the working directory in the container
WORKDIR /code

# copy the content of the local src directory to the working directory
COPY src/ .

# command to run on container start
ENTRYPOINT [ "python", "./simple_flux_query_using_python.py" ] 